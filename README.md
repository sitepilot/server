# Vagrant Development Server
This server is used at Van Egmond Groep to develop our applications. It's basically a virtual Ubuntu 18.04 server with [Openlitespeed](https://openlitespeed.org//), [LSPHP](https://www.litespeedtech.com/open-source/litespeed-sapi/php), [MariaDB](https://mariadb.org/) and development tools pre-installed.

## Requirements 
* [Vagrant](https://www.vagrantup.com/) 
* [VirtualBox](https://www.virtualbox.org/)

## Included software
1. Openlitespeed Webserver
1. PHP 7.2
1. Composer
1. PHPunit
1. MariaDB
1. NodeJS & NPM
1. WPCLI
1. Wordmove

## Usage
1. Clone this repository to your computer.
1. Navigate to the folder in your terminal or command prompt.
1. Run `vagrant up` to start the server.
1. Run `vagrant ssh` to login to the server.
1. Run `vagrant halt` to stop the server.
1. Navigate to `http://192.168.26.100/` to check if the server is running.
1. To manage the webserver configuration navigate to `https://192.168.26.100:7080`, default user: `admin` and password: `secret`. 

## File locations
1. Openlitespeed configuration / installation: `/usr/local/lsws/`.
1. Application files: `/home/vagrant/vhosts/<domain>/public`.
1. Application logs: `/home/vagrant/vhosts/<domain>/logs`.

## Creating a new virtual host
1. Login to the server via ssh using: `vagrant ssh`.
1. Create the application files directory: `mkdir -p /home/vagrant/vhosts/<domain>/public`, replace <domain> with the desired domainname.
1. Create the application logs directory: `mkdir -p /home/vagrant/vhosts/<domain>/logs`, replace <domain> with the disred domainname.
1. Login to the webserver adminpanel: `https://192.168.26.100:7080`.
1. Navigate to `VHost Templates` and click on `App`.
1. Add a new `Member Virtual Host`.
1. Reload the server configuration using the refresh button at the top of the screen.

## Production
To use this configuration in production you have to change the passwords to secure ones in `scripts/provision.sh` before starting the server for the first time (with `vagrant up`). Tip: use [RandomKeygen](https://randomkeygen.com/) to generate passwords.

## Development
We recommend you to use [Visual Studio Code](https://code.visualstudio.com/) with [Remote Development](https://code.visualstudio.com/docs/remote/remote-overview) installed to develop inside the server.

Place the following content in your local ssh-config file:

```
Host server.local
    Hostname 192.168.26.100
    User vagrant
```

And add your public key to authorized_keys (`/home/vagrant/.ssh/authorized_keys`) in the server (login with `vagrant ssh`).
