#!/bin/bash

# Create application dir
mkdir -p /home/vagrant/vhosts/server.local/public
mkdir -p /home/vagrant/vhosts/server.local/logs
cp /home/vagrant/scripts/index.php /home/vagrant/vhosts/server.local/public/index.php

# Create adminer dir
mkdir -p /home/vagrant/vhosts/adminer.local/public
mkdir -p /home/vagrant/vhosts/adminer.local/logs
wget https://www.adminer.org/latest-mysql-en.php -O /home/vagrant/vhosts/adminer.local/public/index.php

# Setup vhost permissions
chown -R vagrant /home/vagrant/vhosts 

# Setup script permissions 
chmod +x /home/vagrant/scripts/*

# Install Openlitespeed
bash /home/vagrant/scripts/ols1clk.sh \
    --adminpassword "secret"  \
    --email "support@sitepilot.io" \
    --lsphp "72" \
    --dbrootpassword "supersecret" \
    --dbname "app" \
    --dbuser "sitepilot" \
    --dbpassword "secret" \
    --listenport 80

# Copy config files
cp /home/vagrant/config/httpd_config.conf /usr/local/lsws/conf/httpd_config.conf
cp /home/vagrant/config/templates/app.conf /usr/local/lsws/conf/templates/app.conf
chown -R lsadm:lsadm /usr/local/lsws/conf 

# Restart webserver
/usr/local/lsws/bin/lswsctrl restart

# Setup PHP symlink 
ln -s /usr/local/lsws/lsphp72/bin/php /usr/local/bin/php

# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
mv composer.phar /usr/local/bin/composer
php -r "unlink('composer-setup.php');"

# Install PHPunit
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
mv phpunit.phar /usr/local/bin/phpunit

# Install NodeJS
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt install -y nodejs

# Install WPCLI
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp

# Install WordMove
apt install -y ruby
gem install wordmove